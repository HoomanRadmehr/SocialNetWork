from django.contrib.auth import login
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from users.auth_backend import EmailBackend
from users.models import Users
from django.contrib.auth.hashers import make_password
from users.serializers import UsersSerializers


class UserSignUp(APIView):
    """
    api end point for user's sign up.
    """
    def post(self,request):
        data = request.data
        ser_data = UsersSerializers(data=data)
        if ser_data.is_valid():
            password = make_password(ser_data.validated_data['password'])
            user = Users(username=ser_data.validated_data['username'], email=ser_data.validated_data['email'],password=password)       
            user.save(request)
            return Response(ser_data.data,status=status.HTTP_201_CREATED)
        else:    
            return Response('not valid data or you sign up before',status=status.HTTP_400_BAD_REQUEST)
            

class UserLogin(APIView):
    def post(self,request):
        data = request.data
        auth_backend = EmailBackend()
        user = auth_backend.authenticate(request,email=data['email'],password=data['password'])
        if user:
            login(request,user)
            return Response('login successfully',status.HTTP_200_OK)
        else:
            return Response('invalid information',status.HTTP_400_BAD_REQUEST)
            
