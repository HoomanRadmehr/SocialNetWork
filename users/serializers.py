from django.db.models import fields
from rest_framework.serializers import ModelSerializer
from users.models import Users

class UsersSerializers(ModelSerializer):
    class Meta:
        model = Users
        fields=['username','email','password']
        