from django.db import models
from django.contrib.auth.models import AbstractUser
from rest_framework.response import Response
from rest_framework import status
from utils.geo_location import get_country
from utils.check_holiday import check_holiday
from utils.validate_email import is_valid_email

class Users(AbstractUser):
    email=models.EmailField(unique=True)
    country = models.CharField(max_length=50,null=True,blank=True)
    sign_up_on_holiday = models.BooleanField(null=True,blank=True)
    
    def save(self,*arg,**kwargs) -> None:
        try:
            request = kwargs['request']
            self.country = get_country(request)
            self.sign_up_on_holiday = check_holiday(request)
            if is_valid_email(self.email):
                return super().save()
            else:
                return Response('not valid email',status=status.HTTP_400_BAD_REQUEST)
        except:
            return super().save()
        