from re import U
from users.models import Users
from django.contrib.auth.hashers import check_password
from django.contrib.auth.backends import ModelBackend

class EmailBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None, **kwargs):
        try:
            user = Users.objects.get(email=email)
        except Users.DoesNotExist:
            return None
        if user:
            if check_password(password,user.password):
                return user
        return None
    