import requests
import datetime
from social.settings import ABSTRACT_APIKEY_HOLIDAY
from utils.geo_location import get_country

def check_holiday(request):
    today = datetime.datetime.now()
    year = today.year
    month = today.month
    day = today.day
    country = get_country(request)
    response = requests.get(f'https://holidays.abstractapi.com/v1/?api_key={ABSTRACT_APIKEY_HOLIDAY}&country={country}&year={year}&month={month}&day={day}')
    if response.content is not None:
        return True
    else:
        return False
    