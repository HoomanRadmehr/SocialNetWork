import requests
import json


from social.settings import ABSTRACT_APIKEY_GEO
from utils.ip import get_ip_address,is_valid_ip

def get_country(request):
    ip = get_ip_address(request)
    if is_valid_ip(ip):
        response = requests.get(f'https://ipgeolocation.abstractapi.com/v1/?api_key={ABSTRACT_APIKEY_GEO}&ip_address={ip}')
        response = response.json()
        country_code = response['country_code']
        return country_code
