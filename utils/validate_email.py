import requests
from social.settings import ABSTRACT_APIKEY_EMAIL

def is_valid_email(email):
    response = requests.get(f'https://emailvalidation.abstractapi.com/v1/?api_key={ABSTRACT_APIKEY_EMAIL}&email={email}')
    response = response.json()
    if response['is_valid_format']['value'] == True:
        return True
    else:
        return False
    