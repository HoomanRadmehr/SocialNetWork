from django.db import models
from users.models import Users
    

class Posts(models.Model):
    tittle = models.CharField(max_length=50)
    body = models.TextField()
    creator = models.ForeignKey(Users,on_delete=models.CASCADE,related_name='post_creator')
    created_at = models.DateTimeField(auto_now_add=True)
    likes = models.ForeignKey(Users,on_delete=models.DO_NOTHING,related_name='post_likes',null=True,blank=True)


class Likes(models.Model):
    TYPES = (('L','like'),('UN','unlike'),)
    
    user = models.ForeignKey(Users,on_delete=models.DO_NOTHING,related_name='like_user')
    post = models.ForeignKey(Posts,on_delete=models.DO_NOTHING,related_name='like_post')
    type = models.CharField(max_length=2,choices=TYPES,null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def save(self,*arg,**kwargs) -> None:
        try:
            like = Likes.objects.get(user = self.user,post=self.post)
        except:
            like = None
        if like:
            return None
        else:
            return super().save()