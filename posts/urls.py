from django.urls import path
from posts import views


app_name = 'posts'
urlpatterns=[
   path('post/',views.PostApi.as_view()),
   path('post/<int:post_id>/',views.RetrivePostApi.as_view()),
   path('like/<int:post_id>/',views.LikeApi.as_view()),
   path('delete_like/<int:post_id>/',views.DeleteLike.as_view()) 
]