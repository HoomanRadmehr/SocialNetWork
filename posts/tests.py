from django.test import TestCase
from posts.models import Posts
from users.models import Users


class TestPosts(TestCase):
    def setUp(self) -> None:
        self.user = Users.objects.create(email="test@gmail.com",password='test')
        self.post = Posts.objects.create(id=1,tittle="test_tittle",body="test_body",creator=self.user)
        
    def test_create_post(self):
        self.assertEqual(self.post.body,'test_body')
        
    def test_retrieve_post(self):
        try:
            post = Posts.objects.get(id=1)
        except Exception as e:
            return str(e)
        self.assertEqual(post.tittle,'test_tittle')
        
    def test_update_post(self):
        post = self.post
        post.tittle = "test_tittle_2"
        post.save()
        self.assertEqual(post.tittle,"test_tittle_2")
        
    def test_delete_post(self):
        count_before_delete = Posts.objects.count()
        self.post.delete()
        count_after_delete = Posts.objects.count()
        self.assertLess(count_after_delete,count_before_delete)
        