from rest_framework.serializers import ModelSerializer
from posts.models import Posts,Likes
from users.serializers import UsersSerializers
   

class LikeSerializer(ModelSerializer):
    class Meta:
        model = Likes
        fields = ['type',]
        
        
class PostSerializer(ModelSerializer):
    class Meta:
        model = Posts
        fields = ['tittle','body']