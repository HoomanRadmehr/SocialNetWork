from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from posts.models import Likes, Posts
from posts.serializers import LikeSerializer, PostSerializer


class RetrivePostApi(APIView):
    """
    API endpoint for get , update and delete post.

    Args:
        id: post id
    """
    permission_classes = [IsAuthenticated,]
    
    def get(self,request,id):
        post = get_object_or_404(Posts,id)
        ser_post = PostSerializer(data=post)
        return Response(ser_post.data,status=status.HTTP_200_OK)
    
    def put(self,request,id):
        post = get_object_or_404(Posts,id=id)
        data = request.data
        ser_data = PostSerializer(data=data)
        if ser_data.is_valid():
            if post.creator == request.user:
                post.update(ser_data.validated_data)
                return Response('update successfully',status.HTTP_200_OK)
            else:
                return Response('you dont have permission',status=status.HTTP_403_FORBIDDEN)
    
    def delete(self,request,post_id):
        post = get_object_or_404(Posts,pk=post_id)
        
        if post.creator == request.user:
            post.delete()
            return Response('deleted',status.HTTP_200_OK)
        else:
            return Response('permission denied',status.HTTP_403_FORBIDDEN)


class PostApi(APIView):
    """
    api end point for create post and list posts.

    Args:
        None
    """
    permission_classes = [IsAuthenticated,]
    
    def get(self,request):
        posts = Posts.objects.all()
        ser_posts = PostSerializer(posts,many=True)
        return Response(ser_posts.data,status=status.HTTP_200_OK)
    
    def post(self,request):
        data = request.data
        ser_data = PostSerializer(data=data)
        if ser_data.is_valid():
            post = Posts(tittle = ser_data.validated_data['tittle'],body=ser_data.validated_data['body'],creator=request.user)
            post.save()
            return Response(ser_data.data,status=status.HTTP_201_CREATED)
        else:
            return Response(ser_data.errors,status=status.HTTP_400_BAD_REQUEST)
        
        
class LikeApi(APIView):
    """
    api end point for create like or unlike posts

    Args:
        None
    """
    permission_classes = [IsAuthenticated,]
    
    def post(self,request,post_id):
        post = get_object_or_404(Posts,id=post_id)
        data = request.data
        ser_data = LikeSerializer(data=data)
        if ser_data.is_valid():
            like = Likes(user = request.user,post=post,type=ser_data.validated_data['type'])
            like.save()
            return Response('liked succesfully',status.HTTP_201_CREATED)
    
            
class DeleteLike(APIView):
    """
    api endpoint for delete a like or unlike.

    Args:
        post_id: post_id
    """
    permission_classes = [IsAuthenticated,]
    
    def delete(self,request,post_id):
        post = get_object_or_404(Posts,id=post_id)
        like = get_object_or_404(Likes,post=post,user=request.user)
        if request.user == like.user:
            like.delete()
            return Response('deleted',status.HTTP_200_OK)
        else:
            return Response('permission denied',status.HTTP_403_FORBIDDEN)
        