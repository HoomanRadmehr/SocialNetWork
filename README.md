# Social Media

Social media is a python webapplication app.

## Set up

clone it from  [git](https://gitlab.com) repository.

```bash
$ git clone https://gitlab.com/HoomanRadmehr/SocialNetWork.git
$ cd sample-django-app
```
Create a virtual environment to install dependencies in and activate it:

```bash
python3 -m venv venv
```
Then install the dependencies:
```bash
(venv)$ pip install -r requirements.txt
```
Once pip has finished downloading the dependencies:
```bash
(venv)$ cd social
(venv)$ python3 manage.py runserver
```

## Tests

```bash
(venv)$ python3 manage.py test
```